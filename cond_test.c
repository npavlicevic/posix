#include "cond_test.h"

void wait_thread_test() {
  pthread_t id;
  struct timespec timeout;
  Wait *wait = wait_create();
  int status = pthread_create(&id, NULL, wait_thread, wait);
  if(status) {
    err_abort(status, "Create thread");
  }
  timeout.tv_sec = time(NULL) + 2;
  timeout.tv_nsec = 0;
  status = pthread_mutex_lock(&(wait->mutex));
  while(!wait->value) {
    status = pthread_cond_timedwait(&(wait->cond), &(wait->mutex), &timeout);
    if(status == ETIMEDOUT) {
      printf("Timed out");
      break;
    } else if(status) {
      err_abort(status, "Wait on condition");
    }
  }
  wait_destroy(&wait);
}
