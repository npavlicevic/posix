#include "cond.h"

Wait *wait_create() {
  Wait *this = (Wait*)calloc(1, sizeof(Wait));
  this->value = 0;
  return this;
}
void *wait_thread(void *arg) {
  sleep(1);
  Wait *wait = (Wait*)arg;
  int status = pthread_mutex_lock(&(wait->mutex));
  if(status) {
    err_abort(status, "Lock mutext");
  }
  wait->value = 1;
  status = pthread_cond_signal(&(wait->cond));
  if(status) {
    err_abort(status, "Signal condition");
  }
  status = pthread_mutex_unlock(&(wait->mutex));
  if(status) {
    err_abort(status, "Unlock mutex");
  }
  return NULL;
}
void wait_destroy(Wait **this) {
  free(*this);
  *this = NULL;
}
