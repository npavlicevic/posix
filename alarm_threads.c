#include "alarm_threads.h"
void* alarm_thread(void *_this) {
  AlarmTag *this = (AlarmTag*)_this;
  int status = pthread_detach(pthread_self());
  if(status != 0) {
    err_abort(status, "Detach thread");
  }
  sleep(this->seconds);
  printf("%d %s\n", this->seconds, this->message);
  free(this);
  return NULL;
}
