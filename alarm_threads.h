#ifndef ALARM_THREADS_H
#define ALARM_THREADS_H
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "errors.h"
#define TRUE 1
#define FALSE 0
typedef struct alarm_tag {
  int seconds;
  char message[64];
} AlarmTag;
void* alarm_thread(void *_this);
#endif
