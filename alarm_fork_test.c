#include "alarm_fork_test.h"

void alarm_fork_test() {
  int seconds;
  pid_t pid;
  char message[64];
  while(TRUE) {
    printf("Alarm>");
    fscanf(stdin, "%d %s", &seconds, message);
    if(strlen(message) <= 1) {
      continue;
    }
    pid = fork();
    if(pid == (pid_t) - 1) {
      errno_abort("Fork");
    }
    if(pid == (pid_t)0) {
      sleep(seconds);
      printf("%d %s\n", seconds, message);
      exit(0);
    } else {
      do {
        pid = waitpid((pid_t)-1, NULL, WNOHANG);
        if(pid == (pid_t)-1) {
          errno_abort("Wait for child");
        }
      } while(pid != (pid_t)0);
    }
  }
}
