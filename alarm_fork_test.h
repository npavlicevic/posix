#ifndef ALARM_FORK_TEST_H
#define ALARM_FORK_TEST_H
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "errors.h"
#include "alarm_fork.h"
void alarm_fork_test();
#endif
