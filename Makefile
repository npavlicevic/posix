#
# Makefile 
# posix
#

CC=gcc
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic -pthread
LIBS=-lm

FILES=alarm_threads.c alarm_threads_test.c main_alarm_threads.c
FILES_ALARM_FORK=alarm_fork_test.c main_alarm_fork.c
FILES_COND=cond.c cond_test.c main_cond.c
CLEAN=main_alarm_threads main_alarm_fork main_cond

all: alarm_threads alarm_fork cond

alarm_threads: ${FILES}
	${CC} ${CFLAGS} $^ -o main_alarm_threads ${LIBS}

alarm_fork: ${FILES_ALARM_FORK}
	${CC} ${CFLAGS} $^ -o main_alarm_fork ${LIBS}

cond: ${FILES_COND}
	${CC} ${CFLAGS} $^ -o main_cond ${LIBS}

clean: ${CLEAN}
	rm $^
