#include "alarm_threads_test.h"

void alarm_threads_test() {
  int status;
  char line[128];
  AlarmTag *alarm;
  pthread_t thread;
  while(TRUE) {
    printf("Alarm>");
    if(fgets(line, sizeof(line), stdin) == NULL) {
      exit(0);
    }
    if(strlen(line) < 1) {
      continue;
    }
    alarm = (AlarmTag*)malloc(sizeof(AlarmTag));
    if(sscanf(line, "%d %64[^\n]", &alarm->seconds, alarm->message) < 2) {
      // parse line into seconds and message
      fprintf(stderr, "Bad message\n");
      free(alarm);
    } else {
      status = pthread_create(&thread, NULL, alarm_thread, alarm);
      if(status != 0) {
        err_abort(status, "Create alarm thread");
      }
    }
  }
}
