#ifndef COND_H
#define COND_H
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include "errors.h"
typedef struct wait {
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  int value;
} Wait;
Wait *wait_create();
void *wait_thread(void *arg);
void wait_destroy(Wait **this);
#endif
